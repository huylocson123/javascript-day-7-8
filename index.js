var arrayNumber = [];


document.getElementById("btn-add").addEventListener("click", function() {
    var number = document.getElementById("number").value * 1;
    arrayNumber.push(number);
    document.getElementById("array").value = arrayNumber;
});

// Tính Tổng
document.getElementById("btn-tong-duong").addEventListener("click", function() {
    var sum = 0;
    for (let index = 0; index < arrayNumber.length; index++) {
        if (arrayNumber[index] > 0) {
            sum += arrayNumber[index];
        }
        document.getElementById("kq-tong-duong").value = sum;
    }
});

// Đếm Số
document.getElementById("btn-count").addEventListener("click", function() {
    count = 0;
    for (let index = 0; index < arrayNumber.length; index++) {
        if (arrayNumber[index] > 0) {
            count++;
        }
        document.getElementById("kq-count").value = "Có: " + count + " Số Dương";
    }
});

// Tìm min
document.getElementById("btn-min").addEventListener("click", function() {
    var min = arrayNumber[0];

    for (let index = 0; index < arrayNumber.length; index++) {

        if (min > arrayNumber[index]) {
            min = arrayNumber[index];
        }
        document.getElementById("kq-min").value = " Số nhỏ nhất: " + min;
    }
});

// tìm so duong nho nhat
document.getElementById("btn-min-pos").addEventListener("click", function() {
    var posArr = [];
    for (let index = 0; index < arrayNumber.length; index++) {
        if (arrayNumber[index] > 0) {
            posArr.push(arrayNumber[index]);
        }
    }
    var minPos = posArr[0];
    if (posArr.length === 0) {
        document.getElementById("kq-min-pos").value = "Không có số dương ";
        console.log(posArr);
    } else {
        for (let y = 0; y < posArr.length; y++) {
            if (minPos > posArr[y]) {
                minPos = posArr[y];

            }
            document.getElementById("kq-min-pos").value = " Số dương nhỏ nhất: " + minPos;
        }
    }
});

// Số chẵn cuối cùng
document.getElementById("btn-last-even").addEventListener("click", function() {
    var lastEven = 0;
    for (let index = 0; index < arrayNumber.length; index++) {
        if (arrayNumber[index] % 2 === 0) {
            lastEven = arrayNumber[index];
        }
        document.getElementById("kq-last-even").value = " Số chẵn cuối cùng: " + lastEven;
    }
});

// Đổi chỗ
document.getElementById("btn-switch").addEventListener("click", function() {
    var pos1 = document.getElementById("pos1").value * 1;
    var pos2 = document.getElementById("pos2").value * 1;
    var tmp = arrayNumber[pos1];
    arrayNumber[pos1] = arrayNumber[pos2];
    arrayNumber[pos2] = tmp;
    document.getElementById("kq-switch").value = arrayNumber;

});

// Sắp xếp tăng dần
document.getElementById("btn-sort").addEventListener("click", function() {
    for (let i = 0; i < arrayNumber.length; i++) {
        for (let j = 0; j < arrayNumber.length; j++) {
            if (arrayNumber[i] < arrayNumber[j]) {
                var tmp = arrayNumber[i];
                arrayNumber[i] = arrayNumber[j];
                arrayNumber[j] = tmp;
            }
        }
        document.getElementById("kq-sort").value = arrayNumber;
    }

});

//  Tìm snt
function checkPrime(n) {
    if (n < 2) {
        return false;
    } else if (n > 2) {
        if (n % 2 == 0) {
            return false;
        }
        for (var i = 3; i < Math.sqrt(n); i += 2) {
            if (n % i == 0) {
                return false;
            }

        }
    }
    return true;
}
document.getElementById("btn-prime").addEventListener("click", function() {

    for (let index = 0; index < arrayNumber.length; index++) {
        if (checkPrime(arrayNumber[index]) == true) {
            document.getElementById("kq-prime").value = arrayNumber[index];
            break;
        } else {
            document.getElementById("kq-prime").value = "Không có số nguyên tố";
        }
    }
});

// Đếm Số Nguyên
var newArray = [];
document.getElementById("btn-add-arr2").addEventListener("click", function() {
    var number2 = document.getElementById("number-2").value * 1;
    newArray.push(number2);
    document.getElementById("array-2").value = newArray;
});
document.getElementById("btn-integer").addEventListener("click", function() {
    var count = 0;
    for (let index = 0; index < newArray.length; index++) {
        if (Number.isInteger(newArray[index]) == true) {
            count++;
            document.getElementById("kq-integer").value = "Có: " + count + " Số nguyên";

        } else {
            document.getElementById("kq-integer").value = "Có: " + count + " Số nguyên";
        }
    }
});

// So Sánh
document.getElementById("btn-compare").addEventListener("click", function() {
    var countPos = 0;
    var countNeg = 0;
    for (let index = 0; index < arrayNumber.length; index++) {
        if (arrayNumber[index] > 0) {
            countPos++;
        } else {
            countNeg++;
        }

    }
    if (countPos > countNeg) {
        document.getElementById("kq-compare").value = "Số dương > Số âm";
    } else if (countPos < countNeg) {
        document.getElementById("kq-compare").value = "Số dương < Số âm";
    } else {
        document.getElementById("kq-compare").value = "Số dương = Số âm";
    }

});